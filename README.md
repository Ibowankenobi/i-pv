# I-PV
Interactive Protein Sequence VIsualization/Viewer
This is the extracted I-PV folder which can also be downloaded from http://i-pv.org/ipv_minimal.html
Refer to the ./1_44/ipv/ReadMe.txt for more detailed explanation on usage.
Follow the video tutorials for how to use the program. 
If you do not like videos, I will also update the wiki. But meanwhile you can visit http://i-pv.org/ipv_rec.html or [here](./1_44_0/ipv/).
I-PV aims to unify protein features in a single interactive figure. It is easy to generate and
highly customizable. Data is checked and then plotted. When you publish figures with I-PV, I recommed you also
post the files in the datatracks folder as supplementary. In I-PV, what you see is what you get, and 
also it makes sure others can get what they see. Below, you will see a sample output. Now, there is interactive,
but than there is [INTERACTIVE](http://i-pv.org/EGFR.html)...

![alt tag](./sample.png)
